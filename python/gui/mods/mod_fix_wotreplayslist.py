﻿import types
import BattleReplay
from gui.app_loader.observers import GameplayStatesObserver
from gui.app_loader.spaces import BattleLoadingSpace
from gui.Scaleform.daapi.view.battle.shared import battle_loading
from gui.Scaleform.daapi.view.battle.shared.crosshair.plugins import SpeedometerWheeledTech
from gui.Scaleform.framework.application import AppEntry
from gui.shared.personality import ServicesLocator
from skeletons.gameplay import GameplayStateID

def override(holder, name, wrapper=None, setter=None):
	"""Override methods, properties, functions, attributes
	:param holder: holder in which target will be overrided
	:param name: name of target to be overriden
	:param wrapper: replacement for override target
	:param setter: replacement for target property setter"""
	if wrapper is None:
		return lambda wrapper, setter=None: override(holder, name, wrapper, setter)
	target = getattr(holder, name)
	wrapped = lambda *a, **kw: wrapper(target, *a, **kw)
	if not isinstance(holder, types.ModuleType) and isinstance(target, types.FunctionType):
		setattr(holder, name, staticmethod(wrapped))
	elif isinstance(target, property):
		prop_getter = lambda *a, **kw: wrapper(target.fget, *a, **kw)
		prop_setter = target.fset if not setter else lambda *a, **kw: setter(target.fset, *a, **kw)
		setattr(holder, name, property(prop_getter, prop_setter, target.fdel))
	else:
		setattr(holder, name, wrapped)

def byteify(data):
	"""Encodes data with UTF-8
	:param data: Data to encode"""
	result = data
	if isinstance(data, dict):
		result = {byteify(key): byteify(value) for key, value in data.iteritems()}
	elif isinstance(data, (list, tuple, set)):
		result = [byteify(element) for element in data]
	elif isinstance(data, unicode):
		result = data.encode('utf-8')
	return result

def isReplaysList():
	""" cheek is playing playList """
	replayCtrl = BattleReplay.g_replayCtrl
	return replayCtrl and replayCtrl._BattleReplay__isPlayingPlayList

@override(BattleLoadingSpace, 'init')
def _init(baseMethod, baseObject):
	""" create battle application if not exist """
	baseMethod(baseObject)
	battleApp = ServicesLocator.appLoader.getDefBattleApp()
	if not battleApp:
		appFactory = ServicesLocator.appLoader._AppLoader__appFactory
		appFactory.createBattle(arenaGuiType = baseObject._arenaGuiType)

@override(battle_loading, 'isBattleLoadingShowed')
def _isBattleLoadingShowed(baseMethod):
	""" in case wotreplayslist fill battle loading window with data """
	if isReplaysList():
		return False
	return baseMethod()

@override(GameplayStatesObserver, 'onStateChanged')
def _onStateChanged(baseMethod, baseObject, stateID, flag, event = None):
	""" ignore client replay state logic """
	if stateID == GameplayStateID.BATTLE_REPLAY:
		stateID = GameplayStateID.OFFLINE
	return baseMethod(baseObject, stateID, flag, event)

@override(SpeedometerWheeledTech, '_SpeedometerWheeledTech__onReplayTimeWarpStart')
def _isBattleLoadingShowed(baseMethod, baseObject):
	""" fix errors from default client """
	if not baseObject.parentObj:
		return
	return baseMethod(baseObject)

@override(AppEntry, 'afterCreate')
def _isBattleLoadingShowed(baseMethod, baseObject):
	""" fix errors from default client """
	try:
		baseMethod(baseObject)
	except:
		pass
